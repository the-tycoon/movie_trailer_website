# Movie Trailer Website
This project is a part of Udacity Full Stack web developer Nanodegree Program

## Run website
1. Clone the repo : https://github.com/immabhay/movie_trailer_website.git
2. Go to terminal (Linux) : Ctrl + T
3. Change directory to the directory where website is cloned (i.e cd movie_trailer_website/ )
4. Type **python entertainment_center.py** 



