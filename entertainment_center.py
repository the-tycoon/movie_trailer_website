import media
import fresh_tomatoes

# Instance iron_man of class Movie
iron_man = media.Movie("Iron Man",
                "Iron Man is a 2008 American superhero film "
                "featuring the Marvel Comics character of the "
                "same name, produced by Marvel Studios and "
                "distributed by Paramount Pictures.",
                "https://upload.wikimedia.org/wikipedia/en/thumb/7/70/Ironmanposter.JPG/220px-Ironmanposter.JPG",   # NOQA
                "https://www.youtube.com/watch?v=8hYlB38asDY")

# Instance Avatar of class Movie
avatar = media.Movie("Avatar",
                "To explore Pandora's biosphere, scientists use "
                "Na'vi-human hybrids called avatars, operated by "
                "genetically matched humans; Jake Sully, a paraplegic "
                "former Marine, replaces his deceased identical twin "
                "brother as an operator of one",
                "https://upload.wikimedia.org/wikipedia/en/b/b0/Avatar-Teaser-Poster.jpg",  # NOQA
                "https://www.youtube.com/watch?v=5PSNL1qE6VY")

# Instance enthiran of class Movie
enthiran = media.Movie("Enthiran",
                "Enthiran (English: Robot) is a 2010 Indian Tamil-language "
                "science fiction film directed by S. Shankar and co-written "
                "by him and Sujatha Rangarajan.",
                "https://upload.wikimedia.org/wikipedia/en/0/0f/Enthiran_poster.jpg",  # NOQA
                "https://www.youtube.com/watch?v=-Ql3A_wiedc")

# Storing movies object/instance in a list
movies_list = [iron_man, avatar, enthiran]

# Calling open_movies_page function
fresh_tomatoes.open_movies_page(movies_list)
