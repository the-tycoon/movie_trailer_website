import webbrowser

class Movie():
        """ Movie class stores details about movies like title of the movie,
        story line , poster image link , youtube trailer link,etc"""

        def __init__(self, title, storyline, poster_image, youtube_trailer):
            self.title = title
            self.movie_storyline = storyline
            self.poster_image_url = poster_image
            self.trailer_youtube_url = youtube_trailer

        def show_trailer(self):
            webbrowser.open(self.trailer_youtube_url)
